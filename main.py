#!/usr/bin/env python3
from plotly.subplots import make_subplots
import plotly.graph_objects as go
from selenium import webdriver
import selenium.common.exceptions
import argparse
import os


# Global Variables
DRIVER_BIN = os.path.join(os.getcwd(), './chromedriver')
BASE_URL = "https://finance.yahoo.com/quote/"
END_URL = "/financials"
COLORS = ['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
          'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
          'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
          'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
          'rgb(188, 189, 34)', 'rgb(23, 190, 207)', 'rgb(75, 86, 125)']

# Scraper functions
def parsing_arguments():
    parser = argparse.ArgumentParser(description="Give statistics for a precise asked good")
    args = parser.parse_args()

    return args


def create_chrome_session(driver, url):
    driver.get(url)

    return driver


def gather_product_info(driver, ticker):
    data = {}
    driver = create_chrome_session(driver, BASE_URL + ticker + END_URL)

    try:
        if driver.find_element_by_xpath("//*[@id=\"lookup-page\"]/section/div/h2/span").text:
            return data
    except selenium.common.exceptions.NoSuchElementException:
        core = driver.find_element_by_id("Col1-1-Financials-Proxy")
        year_list = core.find_elements_by_xpath(".//section/div[4]/div[1]/div[1]/div[1]/div/div")
        revenue_list = core.find_elements_by_xpath(".//section/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div")
        income_list = core.find_elements_by_xpath(".//section/div[4]/div[1]/div[1]/div[2]/div[11]/div[1]/div")

        data["years"] = []
        for index, year in enumerate(year_list):
            if index != 0:
                if year.find_element_by_xpath(".//span").text != "TTM":
                    data["years"].append(int(year.find_element_by_xpath(".//span").text.split("/")[2]))
                else:
                    data["years"].append(year.find_element_by_xpath(".//span").text)

        data["years"].reverse()

        data["revenues"] = []
        for index, revenue in enumerate(revenue_list):
            if index != 0:
                data["revenues"].append(float(revenue.find_element_by_xpath(".//span").text.replace(',', '')))

        data["revenues"].reverse()

        data["incomes"] = []
        for index, income in enumerate(income_list):
            if index != 0:
                data["incomes"].append(float(income.find_element_by_xpath(".//span").text.replace(',', '')))

        data["incomes"].reverse()

    return data


def calculate(revenues, incomes):
    revenue_results = []
    profit_results = []
    for index, revenue in enumerate(revenues):
        if index != 0:
            revenue_results.append(round(((revenue - revenues[index - 1]) / revenues[index - 1] * 100), 2))
        profit_results.append(round((incomes[index] / revenue * 100), 2))

    return revenue_results, profit_results


def display_graph(data, ticker):
    growth, profit = calculate(data["revenues"], data["incomes"])
    data["year_revenues"] = data["years"]
    fig = make_subplots(rows=1, cols=2, subplot_titles=("Revenue growth", "Margin profit"))
    fig.add_trace(go.Scatter(x=data["year_revenues"], y=growth, line=dict(color=COLORS[0], width=2)), row=1, col=1)
    fig.add_trace(go.Scatter(x=data["years"], y=profit, line=dict(color=COLORS[0], width=2)), row=1, col=2)
    fig.update_layout(title="Economics data for " + ticker,
                      xaxis_title="Month",
                      yaxis_title="Percentage")
    fig.show()


def main():
    parsing_arguments()
    data = {}
    ticker = ""
    first_time = True

    while ticker != "exit":
        if not first_time:
            driver = webdriver.Chrome(executable_path=DRIVER_BIN)
            driver.implicitly_wait(30)
        if ticker != "" and ticker != "exit":
            data = gather_product_info(driver, ticker)
        if data == {} and ticker != "":
            print("Please enter a valid ticker")
        if data != {}:
            display_graph(data, ticker)
        ticker = input("$> ")
        first_time = False


if __name__ == "__main__":
    main()

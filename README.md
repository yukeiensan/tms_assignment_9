# Installing requirements
`pip3 install selenium argparse plotly`

# Usage
```
usage: profit_growth.exe [-h]

Give statistics for a precise asked good

optional arguments:
  -h, --help  show this help message and exit
```
